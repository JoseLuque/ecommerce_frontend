<?php

class Ruta{

	/*=============================================
	RUTA LADO DEL CLIENTE
	=============================================*/	

	public function ctrRuta(){

		return "http://localhost/ecommerce_frontend/";
	
	}

	/*=============================================
	RUTA LADO DEL SERVIDOR
	=============================================*/	

	public function ctrRutaServidor(){

		return "http://localhost/ecommerce_backend/";
	
	}

}